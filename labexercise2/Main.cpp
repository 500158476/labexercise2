#include <iostream>
#include <conio.h>
#include <string>
using namespace std;

	enum Rank
	{
		Two = 2,
		Three = 3,
		Four = 4,
		Five = 5,
		Six = 6,
		Seven = 7,
		Eight = 8,
		Nine = 9,
		Ten = 10,
		Jack = 11,
		Queen = 12,
		King = 13,
		Ace = 14
	};

	enum Suit
	{
		Clubs,
		Spades,
		Diamonds,
		Hearts
	};

	struct Card
	{
		Rank cardrank;
		Suit cardsuit;
	};

	void PrintCard(Card card)
	{
		switch (card.cardrank)
		{
		case Two: cout << "The two of "; break;
		case Three: cout << "The three of "; break;
		case Four: cout << "The four of "; break;
		case Five: cout << "The five of "; break;
		case Six: cout << "The six of "; break;
		case Seven: cout << "The seven of "; break;
		case Eight: cout << "The eigjht of "; break;
		case Nine: cout << "The nine of "; break;
		case Ten: cout << "The ten of "; break;
		case King: cout << "The king of "; break;
		case Queen: cout << "The queen of "; break;
		case Jack: cout << "The jack of "; break;
		case Ace: cout << "The ace of "; break;
		}

		switch (card.cardsuit)
		{
		case Diamonds: cout << "Diamonds\n"; break;
		case Hearts: cout << "Hearts\n"; break;
		case Spades: cout << "Spades\n"; break;
		case Clubs: cout << "Clubs\n"; break;
		}

	}

	Card GetHighCard(Card c1, Card c2)
	{
		if (c1.cardrank > c2.cardrank) return c1;

		return c2;
	}

int main()
{
	
	Card c1;
	c1.cardrank = Two;
	c1.cardsuit = Diamonds;
	PrintCard(c1);

	Card c2;
	c1.cardrank = Four;
	c2.cardsuit = Hearts;
	PrintCard(GetHighCard(c1, c2));

	/* if (cardrank1 > cardrank2)
	{
		void print cardrank1;
	}
	
	if (cardrank1 < cardrank 2)
	{
		print cardrank2;
	}

	if (cardrank1 == cardrank2)
	{
		print "Card Values are the same.";
	} */

	_getch();
	return 0;
}